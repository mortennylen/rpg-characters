package no.noroff.characters;

import no.noroff.characters.types.Mage;
import no.noroff.exceptions.InvalidArmorException;
import no.noroff.exceptions.InvalidWeaponException;
import no.noroff.items.*;
import no.noroff.items.enums.ArmorType;
import no.noroff.items.enums.Slot;
import no.noroff.items.enums.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void equipItem_TooHighLevelWeapon_ShouldThrowInvalidWeaponException() {
        //Arrange
        Mage mage = new Mage("");
        Weapon testWeapon = new Weapon("Common Staff",2, WeaponType.STAFF,5,1.7);
        //Act
        //Assert
        assertThrows(InvalidWeaponException.class, () ->  mage.equipItem(testWeapon));
    }

    @Test
    void equipItem_TooHighLevelArmor_ShouldThrowInvalidArmorException() {
        //Arrange
        Mage mage = new Mage("");
        Armor testArmor = new Armor("Common Cloth Body Armor",2, Slot.BODY, ArmorType.CLOTH,0,0,7,2);
        //Act
        //Assert
        assertThrows(InvalidArmorException.class, () ->  mage.equipItem(testArmor));
    }

    @Test
    void equipItem_WrongWeaponType_ShouldThrowInvalidWeaponException() {
        //Arrange
        Mage mage = new Mage("");
        Weapon testWeapon = new Weapon("Common Axe",1, WeaponType.AXE,7,1.1);
        //Act
        //Assert
        assertThrows(InvalidWeaponException.class, () ->  mage.equipItem(testWeapon));
    }

    @Test
    void equipItem_WrongArmorType_ShouldThrowInvalidArmorException() {
        //Arrange
        Mage mage = new Mage("");
        Armor testArmor = new Armor("Common Plate Body Armor",1, Slot.BODY, ArmorType.PLATE,1,0,0,2);
        //Act
        //Assert
        assertThrows(InvalidArmorException.class, () ->  mage.equipItem(testArmor));
    }

    @Test
    void equipItem_ValidWeapon_ShouldReturnTrue() {
        //Arrange
        Mage mage = new Mage("");
        Weapon testWeapon = new Weapon("Common Staff",1, WeaponType.STAFF,4,1.5);
        boolean actual;
        //Act
        try {
            actual = mage.equipItem(testWeapon);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            actual = false;
        }
        //Assert
        assertTrue(actual);
    }

    @Test
    void equipItem_ValidArmor_ShouldReturnTrue() {
        //Arrange
        Mage mage = new Mage("");
        Armor testArmor = new Armor("Common Cloth Body Armor",1, Slot.BODY, ArmorType.CLOTH,0,0,5,1);
        boolean actual;
        //Act
        try {
            actual = mage.equipItem(testArmor);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            actual = false;
        }
        //Assert
        assertTrue(actual);
    }

    @Test
    void calculateDPS_WithNoItemsEquipped() {
        //Arrange
        Mage mage = new Mage("");
        double expected = 1*(1+(8.0/100));
        //Act
        double actual = mage.calculateDPS();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void calculateDPS_WithValidWeapon() {
        //Arrange
        Mage mage = new Mage("");
        Weapon testWeapon = new Weapon("Common Staff",1, WeaponType.STAFF,4,1.5);
        Armor testArmor = new Armor("Common Cloth Body Armor",1, Slot.BODY, ArmorType.CLOTH,0,0,5,1);
        try {
            mage.equipItem(testWeapon);
            mage.equipItem(testArmor);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        double expected = (4.0*1.5)*(1.0+(8.0+5.0)/100);
        //Act
        double actual = mage.calculateDPS();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void calculateDPS_WithValidWeaponAndArmor() {
        //Arrange
        Mage mage = new Mage("");
        Weapon testWeapon = new Weapon("Common Staff",1, WeaponType.STAFF,4,1.5);
        try {
            mage.equipItem(testWeapon);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        double expected = (4.0*1.5)*(1.0+8.0/100);
        //Act
        double actual = mage.calculateDPS();
        //Assert
        assertEquals(expected,actual);
    }

}