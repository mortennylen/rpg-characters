package no.noroff.characters.types;

import no.noroff.characters.PrimaryAttribute;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void newMage_WhenCreated_ShouldBeLevel1() {
        // Arrange
        Mage mage = new Mage("");
        int expected = 1;
        // Act
        int actual = mage.getLevel();
        //Assert
        assertEquals(expected,actual);

    }

    @Test
    void levelUp_NewMageGainsLevel_ShouldBeLevel2() {
        // Arrange
        Mage mage = new Mage("");
        int expected = 2;
        // Act
        mage.levelUp(1);
        int actual = mage.getLevel();
        //Assert
        assertEquals(expected,actual);

    }

    @Test
    void newMage_WhenCreated_HasCorrectBasePrimaryAttributes() {
        // Arrange
        Mage mage = new Mage("");
        //PrimaryAttribute expected = new PrimaryAttribute(1,1,8,5);
        double[] expected = {1,1,8,5};
        // Act
        //PrimaryAttribute actual = mage.getBasePrimaryAttributes();
        double[] actual = mage.getBasePrimaryAttributes().getPrimaryAttributesArray();
        //Assert
        assertArrayEquals(expected,actual);
    }

    @Test
    void levelUp_NewMageGainsLevel_GainsCorrectBasePrimaryAttributes() {
        // Arrange
        Mage mage = new Mage("");
        double[] expected = {2,2,13,8};
        // Act
        mage.levelUp(1);
        double[] actual = mage.getBasePrimaryAttributes().getPrimaryAttributesArray();;
        //Assert
        assertArrayEquals(expected,actual);

    }
}