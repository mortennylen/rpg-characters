# RPGCharacters

RPGCharacters is a console application that creates RPG characters, weapons and armor. 

## Prerequisites

JDK 16 or newer.

## Using RPGCharacters

Characters classes:<br/>
Mage,
Ranger, 
Rogue, 
Warrior,

ex:<br/>
Mage hero = new Mage("name");

Items classes:<br/>
Weapon,
Armor

ex:<br/>
Weapon testWeapon = new Weapon("Common Staff",1, WeaponType.STAFF,7,1.1);<br/>
Armor testArmor = new Armor("Common Cloth Body Armor",1, Slot.BODY, ArmorType.CLOTH,0,0,5,1);

Leveling up character:<br/>
hero.levelUp(int)

## Contributing to RPGCharacters
To contribute to MovieCharacterAPI, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin https://gitlab.com/mortennylen/rpg-characters`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

You name will go here if you contribute. :)

## Contact

Please make an issue with your request.

## License

..
