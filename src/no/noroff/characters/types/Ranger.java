package no.noroff.characters.types;

import no.noroff.characters.Character;
import no.noroff.characters.AttributePoints;
import no.noroff.items.enums.ArmorType;
import no.noroff.items.enums.WeaponType;

import java.util.ArrayList;
import java.util.List;

public class Ranger extends Character {

    // Constants
    public static final ArrayList<WeaponType> RANGER_WEAPONS_ALLOWED = new ArrayList<>(List.of(WeaponType.STAFF, WeaponType.WAND)); // Weapons the class is allowed to equip.
    public static final ArrayList<ArmorType> RANGER_ARMOR_ALLOWED = new ArrayList<>(List.of(ArmorType.CLOTH)); // Armor the class is allowed to equip.
    public static final String RANGER_MAIN_ATTRIBUTE = "dexterity"; // Is used to calculate character DPS

    // Constructor
    public Ranger(String name) {
        super(name,
                AttributePoints.RANGER_STRENGTH,
                AttributePoints.RANGER_DEXTERITY,
                AttributePoints.RANGER_INTELLIGENCE,
                AttributePoints.RANGER_VITALITY,
                RANGER_WEAPONS_ALLOWED,
                RANGER_ARMOR_ALLOWED,
                RANGER_MAIN_ATTRIBUTE);
    }

    // Increase current level by x and increase basePrimaryAttributes
    @Override
    public void levelUp(int x) {
        super.level += x;
        super.basePrimaryAttributes.incrementAttributes(
                x * AttributePoints.RANGER_LEVELUP_STRENGTH,
                x * AttributePoints.RANGER_LEVELUP_DEXTERITY,
                x * AttributePoints.RANGER_LEVELUP_INTELLIGENCE,
                x * AttributePoints.RANGER_LEVELUP_VITALITY);
    }
}
