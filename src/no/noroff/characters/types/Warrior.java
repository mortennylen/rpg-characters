package no.noroff.characters.types;

import no.noroff.characters.Character;
import no.noroff.characters.AttributePoints;
import no.noroff.items.enums.ArmorType;
import no.noroff.items.enums.WeaponType;

import java.util.ArrayList;
import java.util.List;

public class Warrior extends Character {

    // Constants
    public static final ArrayList<WeaponType> WARRIOR_WEAPONS_ALLOWED = new ArrayList<>(List.of(WeaponType.STAFF, WeaponType.WAND)); // Weapons the class is allowed to equip.
    public static final ArrayList<ArmorType> WARRIOR_ARMOR_ALLOWED = new ArrayList<>(List.of(ArmorType.CLOTH)); // Armor the class is allowed to equip.
    public static final String WARRIOR_MAIN_ATTRIBUTE = "strength"; // Is used to calculate character DPS

    // Constructor
    public Warrior(String name) {
        super(name,
                AttributePoints.WARRIOR_STRENGTH,
                AttributePoints.WARRIOR_DEXTERITY,
                AttributePoints.WARRIOR_INTELLIGENCE,
                AttributePoints.WARRIOR_VITALITY,
                WARRIOR_WEAPONS_ALLOWED,
                WARRIOR_ARMOR_ALLOWED,
                WARRIOR_MAIN_ATTRIBUTE);
    }

    // Increase current level by x and increase basePrimaryAttributes
    @Override
    public void levelUp(int x) {
        super.level += x;
        super.basePrimaryAttributes.incrementAttributes(
                x * AttributePoints.WARRIOR_LEVELUP_STRENGTH,
                x * AttributePoints.WARRIOR_LEVELUP_DEXTERITY,
                x * AttributePoints.WARRIOR_LEVELUP_INTELLIGENCE,
                x * AttributePoints.WARRIOR_LEVELUP_VITALITY);
    }
}
