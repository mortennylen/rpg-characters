package no.noroff.characters.types;

import no.noroff.characters.Character;
import no.noroff.characters.AttributePoints;
import no.noroff.items.enums.ArmorType;
import no.noroff.items.enums.WeaponType;

import java.util.ArrayList;
import java.util.List;

public class Rogue extends Character {

    // Constants
    public static final ArrayList<WeaponType> ROGUE_WEAPONS_ALLOWED = new ArrayList<>(List.of(WeaponType.STAFF, WeaponType.WAND)); // Weapons the class is allowed to equip.
    public static final ArrayList<ArmorType> ROGUE_ARMOR_ALLOWED = new ArrayList<>(List.of(ArmorType.CLOTH)); // Armor the class is allowed to equip.
    public static final String ROGUE_MAIN_ATTRIBUTE = "dexterity"; // Is used to calculate character DPS

    // Constructor
    public Rogue(String name) {
        super(name,
                AttributePoints.ROGUE_STRENGTH,
                AttributePoints.ROGUE_DEXTERITY,
                AttributePoints.ROGUE_INTELLIGENCE,
                AttributePoints.ROGUE_VITALITY,
                ROGUE_WEAPONS_ALLOWED,
                ROGUE_ARMOR_ALLOWED,
                ROGUE_MAIN_ATTRIBUTE);
    }

    // Increase current level by x and increase basePrimaryAttributes
    @Override
    public void levelUp(int x) {
        super.level += x;
        super.basePrimaryAttributes.incrementAttributes(
                x * AttributePoints.ROGUE_LEVELUP_STRENGTH,
                x * AttributePoints.ROGUE_LEVELUP_DEXTERITY,
                x * AttributePoints.ROGUE_LEVELUP_INTELLIGENCE,
                x * AttributePoints.ROGUE_LEVELUP_VITALITY);
    }
}
