package no.noroff.characters.types;

import no.noroff.characters.Character;
import no.noroff.characters.AttributePoints;
import no.noroff.items.enums.ArmorType;
import no.noroff.items.enums.WeaponType;

import java.util.ArrayList;
import java.util.List;

public class Mage extends Character {

    // Constants
    public static final ArrayList<WeaponType> MAGE_WEAPONS_ALLOWED = new ArrayList<>(List.of(WeaponType.STAFF, WeaponType.WAND)); // Weapons the class is allowed to equip.
    public static final ArrayList<ArmorType> MAGE_ARMOR_ALLOWED = new ArrayList<>(List.of(ArmorType.CLOTH)); // Armor the class is allowed to equip.
    public static final String MAGE_MAIN_ATTRIBUTE = "intelligence"; // Is used to calculate character DPS

    // Constructor
    public Mage(String name) {
        super(name,
                AttributePoints.MAGE_STRENGTH,
                AttributePoints.MAGE_DEXTERITY,
                AttributePoints.MAGE_INTELLIGENCE,
                AttributePoints.MAGE_VITALITY,
                MAGE_WEAPONS_ALLOWED,
                MAGE_ARMOR_ALLOWED,
                MAGE_MAIN_ATTRIBUTE);
    }

    // Increase current level by x and increase basePrimaryAttributes
    @Override
    public void levelUp(int x) {
        super.level += x;
        super.basePrimaryAttributes.incrementAttributes(
                x * AttributePoints.MAGE_LEVELUP_STRENGTH,
                x * AttributePoints.MAGE_LEVELUP_DEXTERITY,
                x * AttributePoints.MAGE_LEVELUP_INTELLIGENCE,
                x * AttributePoints.MAGE_LEVELUP_VITALITY);
    }
}
