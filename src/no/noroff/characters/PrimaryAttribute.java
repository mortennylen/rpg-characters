package no.noroff.characters;

public class PrimaryAttribute {
    // Fields
    protected double strength;
    protected double dexterity;
    protected double intelligence;
    protected double vitality;

    // Constructor
    public PrimaryAttribute(double strength, double dexterity, double intelligence, double vitality) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }

    // Getters
    public double[] getPrimaryAttributesArray() {
        return new double[] {strength, dexterity, intelligence, vitality};
    }

    public double getPrimaryAttributeWithString(String attribute) {
        switch (attribute) {
            case "strength":
                return this.strength;
            case "dexterity":
                return this.dexterity;
            case "intelligence":
                return this.intelligence;
            case "vitality":
                return this.vitality;
        }
        return 0;
    }

    public double getStrength() {
        return strength;
    }

    public double getDexterity() {
        return dexterity;
    }

    public double getIntelligence() {
        return intelligence;
    }

    public double getVitality() {
        return vitality;
    }

    //Increase attributes
    public void incrementAttributes(double strength, double dexterity, double intelligence, double vitality) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
        this.vitality += vitality;
    }

    @Override
    public String toString() {
        return "PrimaryAttribute{" +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                ", vitality=" + vitality +
                '}';
    }



}
