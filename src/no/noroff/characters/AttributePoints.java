package no.noroff.characters;

public class AttributePoints {

    // Mage: base stats.
    public static final double MAGE_STRENGTH = 1;
    public static final double MAGE_DEXTERITY = 1;
    public static final double MAGE_INTELLIGENCE = 8;
    public static final double MAGE_VITALITY = 5;

    // Mage: attribute incrementation when level up.
    public static final double MAGE_LEVELUP_STRENGTH = 1;
    public static final double MAGE_LEVELUP_DEXTERITY = 1;
    public static final double MAGE_LEVELUP_INTELLIGENCE = 5;
    public static final double MAGE_LEVELUP_VITALITY = 3;

    // Ranger: base stats.
    public static final double RANGER_STRENGTH = 1;
    public static final double RANGER_DEXTERITY = 7;
    public static final double RANGER_INTELLIGENCE = 1;
    public static final double RANGER_VITALITY = 8;

    // Ranger: attribute incrementation when level up.
    public static final double RANGER_LEVELUP_STRENGTH = 1;
    public static final double RANGER_LEVELUP_DEXTERITY = 5;
    public static final double RANGER_LEVELUP_INTELLIGENCE = 1;
    public static final double RANGER_LEVELUP_VITALITY = 2;

    // Rogue: base stats.
    public static final double ROGUE_STRENGTH = 2;
    public static final double ROGUE_DEXTERITY = 6;
    public static final double ROGUE_INTELLIGENCE = 1;
    public static final double ROGUE_VITALITY = 8;

    // Rogue: attribute incrementation when level up.
    public static final double ROGUE_LEVELUP_STRENGTH = 1;
    public static final double ROGUE_LEVELUP_DEXTERITY = 4;
    public static final double ROGUE_LEVELUP_INTELLIGENCE = 1;
    public static final double ROGUE_LEVELUP_VITALITY = 3;

    // Warrior: base stats.
    public static final double WARRIOR_STRENGTH = 2;
    public static final double WARRIOR_DEXTERITY = 6;
    public static final double WARRIOR_INTELLIGENCE = 1;
    public static final double WARRIOR_VITALITY = 8;

    // Warrior: attribute incrementation when level up.
    public static final double WARRIOR_LEVELUP_STRENGTH = 1;
    public static final double WARRIOR_LEVELUP_DEXTERITY = 4;
    public static final double WARRIOR_LEVELUP_INTELLIGENCE = 1;
    public static final double WARRIOR_LEVELUP_VITALITY = 3;

}
