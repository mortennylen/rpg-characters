package no.noroff.characters;

import java.util.ArrayList;
import java.util.HashMap;

import no.noroff.exceptions.InvalidWeaponException;
import no.noroff.exceptions.InvalidArmorException;

import no.noroff.items.Armor;
import no.noroff.items.Item;
import no.noroff.items.Weapon;
import no.noroff.items.enums.Slot;
import no.noroff.items.enums.ArmorType;
import no.noroff.items.enums.WeaponType;


public abstract class Character {
    // Fields
    protected String name;
    protected int level;
    protected PrimaryAttribute basePrimaryAttributes; // fields: double strength, double dexterity, double intelligence, double vitality
    protected double totalPrimaryAttribute;
    protected String mainAttribute; // "Strength" for Warrior class, "intelligence" for Mage class. "dexterity" for Rangers and Rouges.
    protected ArrayList<WeaponType> weaponsAllowed;
    protected ArrayList<ArmorType> armorAllowed;
    private HashMap<Slot, Item> equipment;

    // Constructor
    public Character(String name, double strength, double dexterity, double intelligence, double vitality, ArrayList<WeaponType> weaponsAllowed, ArrayList<ArmorType> armorAllowed, String damageAttribute) {
        this.name = name;
        this.level = 1;
        this.basePrimaryAttributes = new PrimaryAttribute(strength, dexterity, intelligence, vitality);
        this.weaponsAllowed = weaponsAllowed;
        this.armorAllowed = armorAllowed;
        this.mainAttribute = damageAttribute;
        this.equipment = new HashMap<>();
    }

    // Abstract methods
    public abstract void levelUp(int x);

    // Getters
    public int getLevel() {
        return level;
    }

    public PrimaryAttribute getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", basePrimaryAttributes=" + basePrimaryAttributes +
                ", totalPrimaryAttribute=" + totalPrimaryAttribute +
                '}';
    }

    // Equips items. Returns true if successful. Throws relevant exception if item is incompatible with character level or type.
    public boolean equipItem(Item item) throws InvalidWeaponException, InvalidArmorException {
        switch (item.getSlot()) {
            case WEAPON:
                if (item.getRequiredLevel() > this.level) {
                    throw new InvalidWeaponException("This weapon requires level " + item.getRequiredLevel() + ". You are level " + this.level + ".");
                } else if (!this.weaponsAllowed.contains(item.getType())) {
                    throw new InvalidWeaponException("Your character cannot equip weapons of the type " + item.getType() + ".");
                } else {
                    this.equipment.put(item.getSlot(),item);
                    return true;
                }
            case HEAD, BODY, LEGS:
                if (item.getRequiredLevel() > this.level) {
                    throw new InvalidArmorException("This armor piece requires level " + item.getRequiredLevel() + ". You are level " + this.level + ".");
                } else if (!this.armorAllowed.contains(item.getType())) {
                    throw new InvalidArmorException("Your character cannot equip armor of the type " + item.getType() + ".");
                } else {
                    this.equipment.put(item.getSlot(),item);
                    return true;
                }
        }
        return false;
    }

    // Calculate and returns character DPS
    public double calculateDPS() {
        this.totalPrimaryAttribute = this.basePrimaryAttributes.getPrimaryAttributeWithString(this.mainAttribute);
        // weaponsDPS is 1 pr default.
        double weaponDPS = 1;

        if (!this.equipment.isEmpty()) { // checks if HashMap is empty.

            // Calculate bonusAttributes i.e. total armor bonus attributes.
            PrimaryAttribute bonusAttributes = calculateBonusAttributes();
            // Adds relevant bonus attribute to totalPrimaryAttributes.
            this.totalPrimaryAttribute += bonusAttributes.getPrimaryAttributeWithString(this.mainAttribute);

            // gets the weapon DPS.
            if (this.equipment.get(Slot.WEAPON) != null) {
                Weapon weapon = (Weapon) this.equipment.get(Slot.WEAPON);
                weaponDPS = weapon.getDps();
            }
        }
        return (weaponDPS * (1 + this.totalPrimaryAttribute/100));
    }

    // Calculate and returns bonusAttributes i.e. total armor bonus attributes.
    private PrimaryAttribute calculateBonusAttributes() {
        PrimaryAttribute bonusAttributes = new PrimaryAttribute(0, 0, 0, 0);
        if (!this.equipment.isEmpty()) { // checks if HashMap is empty.

            // Iterates through HashMap equipment using lambda expression.
            this.equipment.forEach((slot, item) -> {
                if (slot != Slot.WEAPON) { // For all non weapons i.e. armor.
                    Armor armor = (Armor) item; //Type Casts Item objects to Armor objects.
                    // adds relevant armor attribute to totalPrimaryAttributes.
                    bonusAttributes.incrementAttributes(
                            armor.getAttributes().getStrength(),
                            armor.getAttributes().getDexterity(),
                            armor.getAttributes().getIntelligence(),
                            armor.getAttributes().getVitality()
                    );
                }
            });
        }
        return bonusAttributes;
    }
    // Returns character stats as String.
    public String getCharacterStats() {
        // Calculate bonusAttributes i.e. total armor bonus attributes.
        PrimaryAttribute bonusAttributes = calculateBonusAttributes();

        // Calculate Character DPS
        double dps = calculateDPS();

        StringBuilder stats = new StringBuilder();

        stats.append("Name:").append(" ").append(this.name);
        stats.append(System.getProperty("line.separator"));
        stats.append("Level:").append(" ").append(this.level);
        stats.append(System.getProperty("line.separator"));
        stats.append("Strength:").append(" ").append(getBasePrimaryAttributes().strength + bonusAttributes.getStrength());
        stats.append(System.getProperty("line.separator"));
        stats.append("Dexterity:").append(" ").append(getBasePrimaryAttributes().dexterity + bonusAttributes.getDexterity());
        stats.append(System.getProperty("line.separator"));
        stats.append("Intelligence:").append(" ").append(getBasePrimaryAttributes().intelligence + bonusAttributes.getIntelligence());
        stats.append(System.getProperty("line.separator"));
        stats.append("Vitality:").append(" ").append(getBasePrimaryAttributes().vitality + bonusAttributes.getVitality());
        stats.append(System.getProperty("line.separator"));
        stats.append("DPS:").append(" ").append(dps);

        return stats.toString();
    }

}
