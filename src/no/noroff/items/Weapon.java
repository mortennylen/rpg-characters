package no.noroff.items;

import no.noroff.items.enums.Slot;
import no.noroff.items.enums.WeaponType;

public class Weapon extends Item {
    // Fields
    protected WeaponType weaponType;
    protected double damage;
    protected double attackSpeed;
    private double dps;

    //Constructor
    public Weapon(String name, int requiredLevel, WeaponType weaponType, double damage, double attackSpeed) {
        super(name, requiredLevel, Slot.WEAPON, weaponType);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.dps = this.damage * this.attackSpeed;
    }

    public double getDps() {
        return dps;
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "name='" + name + '\'' +
                ", requiredLevel=" + requiredLevel +
                ", slot=" + slot +
                ", weaponType=" + weaponType +
                ", damage=" + damage +
                ", attackSpeed=" + attackSpeed +
                ", dps=" + dps +
                '}';
    }

}
