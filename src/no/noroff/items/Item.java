package no.noroff.items;

import no.noroff.items.enums.Slot;

public abstract class Item {
    // Fields
    protected String name;
    protected int requiredLevel;
    protected Slot slot;
    protected Object type; // This could be instantiated as either the WeaponType object or the ArmorType object.

    // Constructors
    public Item(String name, int requiredLevel, Slot slot, Object type) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
        this.type = type;
    }

    public Item() {
    }

    // Getters
    public Object getType() { return type; }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }
}

