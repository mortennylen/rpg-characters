package no.noroff.items;

import no.noroff.characters.PrimaryAttribute;
import no.noroff.items.enums.ArmorType;
import no.noroff.items.enums.Slot;

public class Armor extends Item{
    // Fields
    protected ArmorType armorType;
    protected PrimaryAttribute attributes;

    // Constructor
    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType, double strength, double dexterity, double intelligence, double vitality) {
        super(name, requiredLevel, slot, armorType);
        this.armorType = armorType;
        this.attributes = new PrimaryAttribute(strength,dexterity,intelligence,vitality);
    }
    // Getters
    public PrimaryAttribute getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return "Armor{" +
                "armorType=" + armorType +
                ", attributes=" + attributes +
                ", name='" + name + '\'' +
                ", requiredLevel=" + requiredLevel +
                ", slot=" + slot +
                ", type=" + type +
                '}';
    }
}
