package no.noroff.items.enums;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
