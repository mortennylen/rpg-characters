package no.noroff.items.enums;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
