package no.noroff;

import no.noroff.characters.Character;
import no.noroff.characters.types.Mage;
import no.noroff.exceptions.InvalidWeaponException;
import no.noroff.items.Armor;
import no.noroff.items.Weapon;
import no.noroff.items.enums.ArmorType;
import no.noroff.items.enums.Slot;
import no.noroff.items.enums.WeaponType;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Character> heroes = new ArrayList<>();

        // Mage
        heroes.add(new Mage("John"));

        for (Character hero: heroes) {
            System.out.println(hero.toString());
            hero.levelUp(1);
            System.out.println(hero.getCharacterStats());
            Weapon testWeapon = new Weapon("Common Staff",1, WeaponType.STAFF,7,1.1);
            Armor testArmor = new Armor("Common Cloth Body Armor",1, Slot.BODY, ArmorType.CLOTH,0,0,5,1);
            System.out.println(testWeapon);

            try {
                hero.equipItem(testWeapon);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            System.out.println(testArmor);
            try {
                hero.equipItem(testArmor);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            System.out.println(hero.getCharacterStats());

        }

    }
}
